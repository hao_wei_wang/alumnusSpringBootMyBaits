package com.whw.alumnus.mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

import com.whw.alumnus.entity.Activity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ActivityMapper {
    int deleteByPrimaryKey(Integer actId);

    int insertSelective(Activity record);

    Activity selectByPrimaryKey(Integer actId);

    int updateByPrimaryKeySelective(Activity record);

    List<Activity> selectByActPartIdAndActStatusNot(@Param("actPartId")String actPartId,@Param("actStatus")String actStatus);

    List<Activity> selectByActUserId(@Param("actUserId")String actUserId);


}