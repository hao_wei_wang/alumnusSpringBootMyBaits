package com.whw.alumnus.mapper;

import com.whw.alumnus.entity.Part;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PartMapper {
    int deleteByPrimaryKey(Integer partId);

    int insertSelective(Part record);

    Part selectByPrimaryKey(Integer partId);

    int updateByPrimaryKeySelective(Part record);
}