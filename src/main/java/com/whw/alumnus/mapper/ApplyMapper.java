package com.whw.alumnus.mapper;

import com.whw.alumnus.entity.Apply;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ApplyMapper {
    int deleteByPrimaryKey(Integer applyId);

    int insertSelective(Apply record);

    Apply selectByPrimaryKey(Integer applyId);

    int updateByPrimaryKeySelective(Apply record);
}