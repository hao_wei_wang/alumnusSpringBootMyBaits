package com.whw.alumnus.mapper;

import com.whw.alumnus.entity.Job;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface JobMapper {
    int deleteByPrimaryKey(Integer jobId);

    int insertSelective(Job record);

    Job selectByPrimaryKey(Integer jobId);

    int updateByPrimaryKeySelective(Job record);
}