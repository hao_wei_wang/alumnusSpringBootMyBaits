package com.whw.alumnus.mapper;

import com.whw.alumnus.entity.Clazz;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ClazzMapper {
    int deleteByPrimaryKey(Integer clazzId);

    int insertSelective(Clazz record);

    Clazz selectByPrimaryKey(Integer clazzId);

    int updateByPrimaryKeySelective(Clazz record);
}