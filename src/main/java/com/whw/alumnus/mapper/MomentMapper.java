package com.whw.alumnus.mapper;

import com.whw.alumnus.entity.Moment;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MomentMapper {
    int deleteByPrimaryKey(Integer momentId);

    int insertSelective(Moment record);

    Moment selectByPrimaryKey(Integer momentId);

    int updateByPrimaryKeySelective(Moment record);
}