package com.whw.alumnus.mapper;

import com.whw.alumnus.entity.StarLike;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface StarLikeMapper {
    int deleteByPrimaryKey(Integer id);

    int insertSelective(StarLike record);

    StarLike selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(StarLike record);
}