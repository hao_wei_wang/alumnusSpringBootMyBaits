package com.whw.alumnus.mapper;

import com.whw.alumnus.entity.Logs;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LogsMapper {
    int deleteByPrimaryKey(Integer logsId);

    int insertSelective(Logs record);

    Logs selectByPrimaryKey(Integer logsId);

    int updateByPrimaryKeySelective(Logs record);
}