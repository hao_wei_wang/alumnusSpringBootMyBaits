package com.whw.alumnus.mapper;

import com.whw.alumnus.entity.News;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface NewsMapper {
    int deleteByPrimaryKey(Integer newsId);

    int insertSelective(News record);

    News selectByPrimaryKey(Integer newsId);

    int updateByPrimaryKeySelective(News record);
}