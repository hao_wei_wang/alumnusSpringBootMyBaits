package com.whw.alumnus.mapper;

import com.whw.alumnus.entity.Knowledge;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface KnowledgeMapper {
    int deleteByPrimaryKey(Integer knowledgeId);

    int insertSelective(Knowledge record);

    Knowledge selectByPrimaryKey(Integer knowledgeId);

    int updateByPrimaryKeySelective(Knowledge record);
}