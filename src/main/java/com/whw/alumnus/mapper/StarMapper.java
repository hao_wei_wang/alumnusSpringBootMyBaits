package com.whw.alumnus.mapper;

import com.whw.alumnus.entity.Star;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface StarMapper {
    int deleteByPrimaryKey(Integer starId);

    int insertSelective(Star record);

    Star selectByPrimaryKey(Integer starId);

    int updateByPrimaryKeySelective(Star record);
}