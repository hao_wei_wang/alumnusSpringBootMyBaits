package com.whw.alumnus.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value="com-whw-alunmus-entity-Part")
@Data
public class Part {
    /**
    * 学部编号
    */
    @ApiModelProperty(value="学部编号")
    private Integer partId;

    /**
    * 学部名称
    */
    @ApiModelProperty(value="学部名称")
    private String partName;

    /**
    * 学部负责人姓名
    */
    @ApiModelProperty(value="学部负责人姓名")
    private String partPrincipalName;

    /**
    * 学部负责人电话
    */
    @ApiModelProperty(value="学部负责人电话")
    private String partPrincipaiPhone;
}