package com.whw.alumnus.entity;

import lombok.Data;

/**
* @Author: Wls
* @Date: 23:48 2020/5/9
* @Description: 存放token的解析信息
*/
@Data
public class TokenInfo {
    private String userId;
    private String userOpenId;
    private String userClassId;
    private String userPartId;
    private String userType;
}
