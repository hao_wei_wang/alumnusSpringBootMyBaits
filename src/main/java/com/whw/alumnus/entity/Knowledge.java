package com.whw.alumnus.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.Data;

@ApiModel(value="com-whw-alunmus-entity-Knowledge")
@Data
public class Knowledge {
    /**
    * 知识编号
    */
    @ApiModelProperty(value="知识编号")
    private Integer knowledgeId;

    /**
    * 知识分享者编号
    */
    @ApiModelProperty(value="知识分享者编号")
    private String knowledgeUserId;

    /**
    * 知识内容
    */
    @ApiModelProperty(value="知识内容")
    private String knowledgeContent;

    /**
    * 知识分享时间
    */
    @ApiModelProperty(value="知识分享时间")
    private Date knowledgeTime;

    /**
    * 知识所属学部编号
    */
    @ApiModelProperty(value="知识所属学部编号")
    private Integer knowledgePartId;
}