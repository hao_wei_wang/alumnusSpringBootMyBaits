package com.whw.alumnus.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value="com-whw-alunmus-entity-Clazz")
@Data
public class Clazz {
    /**
    * 班级编号
    */
    @ApiModelProperty(value="班级编号")
    private Integer clazzId;

    /**
    * 班级名称
    */
    @ApiModelProperty(value="班级名称")
    private String clazzName;
}