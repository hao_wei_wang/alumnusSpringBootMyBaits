package com.whw.alumnus.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.Data;

@ApiModel(value="com-whw-alunmus-entity-Logs")
@Data
public class Logs {
    /**
    * 日志编号
    */
    @ApiModelProperty(value="日志编号")
    private Integer logsId;

    /**
    * 日志标题
    */
    @ApiModelProperty(value="日志标题")
    private String logsTitle;

    /**
    * 主要内容
    */
    @ApiModelProperty(value="主要内容")
    private String logsContent;

    /**
    * 创建日期
    */
    @ApiModelProperty(value="创建日期")
    private Date logsTime;
}