package com.whw.alumnus.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.Data;

@ApiModel(value="com-whw-alunmus-entity-Moment")
@Data
public class Moment {
    /**
    * 瞬间编号
    */
    @ApiModelProperty(value="瞬间编号")
    private Integer momentId;

    /**
    * 瞬间发布者编号
    */
    @ApiModelProperty(value="瞬间发布者编号")
    private String momentUserId;

    /**
    * 瞬间内容
    */
    @ApiModelProperty(value="瞬间内容")
    private String momentContent;

    /**
    * 瞬间内容图片
    */
    @ApiModelProperty(value="瞬间内容图片")
    private String momentContentImg;

    /**
    * 瞬间状态(0未审核1已审核)
    */
    @ApiModelProperty(value="瞬间状态(0未审核1已审核)")
    private String momentStatus;

    /**
    * 瞬间发布时间
    */
    @ApiModelProperty(value="瞬间发布时间")
    private Date momentTime;
}