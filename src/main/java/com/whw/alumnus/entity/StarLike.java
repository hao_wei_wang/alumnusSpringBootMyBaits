package com.whw.alumnus.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(value="com-whw-alunmus-entity-StarLike")
@Data
public class StarLike {
    /**
    * 记录编号
    */
    @ApiModelProperty(value="记录编号")
    private Integer id;

    /**
    * 明星编号
    */
    @ApiModelProperty(value="明星编号")
    private Integer starId;

    /**
    * 用户编号
    */
    @ApiModelProperty(value="用户编号")
    private String userId;
}