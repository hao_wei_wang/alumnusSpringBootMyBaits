package com.whw.alumnus.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import lombok.Data;

@ApiModel(value="com-whw-alunmus-entity-UserInfo")
@Data
public class UserInfo {
    /**
    * 用户编号（学号）
    */
    @ApiModelProperty(value="用户编号（学号）")
    private String userId;

    /**
    * 用户姓名
    */
    @ApiModelProperty(value="用户姓名")
    private String userName;

    /**
    * 用户性别
    */
    @ApiModelProperty(value="用户性别")
    private String userSex;

    /**
    * 生源地（省市区）
    */
    @ApiModelProperty(value="生源地（省市区）")
    private String userOrigin;

    /**
    * 班级名称
    */
    @ApiModelProperty(value="班级名称")
    private String userClassName;

    /**
    * 学部名称
    */
    @ApiModelProperty(value="学部名称")
    private String userPartName;

    /**
    * 用户级（入学年份）
    */
    @ApiModelProperty(value="用户级（入学年份）")
    private String userLevel;

    /**
    * 用户生日
    */
    @ApiModelProperty(value="用户生日")
    private Date userBirthday;

    /**
    * 用户手机号
    */
    @ApiModelProperty(value="用户手机号")
    private String userPhone;

    /**
    * 用户邮箱
    */
    @ApiModelProperty(value="用户邮箱")
    private String userEmail;

    /**
    * 上一次资料更新时间
    */
    @ApiModelProperty(value="上一次资料更新时间")
    private Date userUpdateTime;
}