package com.whw.alumnus.controller;

import com.whw.alumnus.entity.Knowledge;
import com.whw.alumnus.entity.Message;
import com.whw.alumnus.service.KnowledgeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@Api(tags = "知识分享模块")
@RequestMapping("api/knowledge")
public class KnowledgeController {



    @Resource
    private KnowledgeService knowledgeService;



    @PostMapping("/addKnowledge")
    @ApiOperation(value = "分享知识")
    public Message<Knowledge> addKnowledge(Knowledge knowledge){
        return knowledgeService.addKnowledge(knowledge);
    }


    @GetMapping("/getKnowledgeByKnowledgeId/{knowledgeId}")
    @ApiOperation(value = "根据知识编号查找知识")
    public Message<Knowledge> getKnowledgeByKnowledgeId(@PathVariable("knowledgeId") int knowledgeId){
        return knowledgeService.getKnowledgeByKnowledgeId(knowledgeId);
    }


    @PostMapping("/deleteKnowledgeByKnowledgeId/{knowledgeId}")
    @ApiOperation(value = "根据知识编号，删除知识信息")
    public Message deleteKnowledgeByKnowledgeId(@PathVariable("knowledgeId")int knowledgeId){
        return knowledgeService.deleteKnowledgeByKnowledgeId(knowledgeId);
    }


    @GetMapping("/getKnowledges")
    @ApiOperation(value = "获取全部知识信息")
    public Message<List<Knowledge>> getKnowledges(){
        return knowledgeService.getKnowledges();
    }


    @GetMapping("/getKnowledgeByKnowledgeUserId/{knowledgeUserId}")
    @ApiOperation(value = "根据发布者编号，查找知识信息")
    public Message<List<Knowledge>> getKnowledgeByKnowledgeUserId(@PathVariable("knowledgeUserId")String knowledgeUserId){
        return knowledgeService.getKnowledgeByKnowledgeUserId(knowledgeUserId);
    }


    @GetMapping("/getKnowledgesByKnowledgePartId/{knowledgePartId}")
    @ApiOperation(value = "根据学部编号，查找知识信息")
    public Message<List<Knowledge>> getKnowledgesByKnowledgePartId(@PathVariable("knowledgePartId")int knowledgePartId){
        return knowledgeService.getKnowledgesByKnowledgePartId(knowledgePartId);
    }

}
