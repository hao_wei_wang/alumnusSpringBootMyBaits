package com.whw.alumnus.controller;


import com.whw.alumnus.config.GlobalConfig;
import com.whw.alumnus.entity.Message;
import com.whw.alumnus.utils.GlobalFileUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("/api/file")
@Api(tags = "全局文件上传模块")
public class GlobalFileController {

    @Resource
    private GlobalConfig config;


    @PostMapping("/")
    @ApiOperation(value = "文件上传，返回文件访问链接")
    public Message fileUpload(MultipartFile[] files) throws IOException {
        if (files.length>0) {
            String rs=config.hostUrl+GlobalFileUtils.saveFileToTempDir(config.globalFilePath,files);
            return Message.success(null).add(rs);

        }
        return Message.fail("没有读取到您的上传文件信息");
    }
}
