package com.whw.alumnus.controller;

import cn.hutool.core.util.StrUtil;
import com.whw.alumnus.entity.Message;
import com.whw.alumnus.entity.TokenInfo;
import com.whw.alumnus.entity.UserInfo;
import com.whw.alumnus.exception.TokenNotExistsExpectation;
import com.whw.alumnus.service.UserInfoService;
import com.whw.alumnus.utils.JwtUtil;
import io.jsonwebtoken.Claims;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@Api(tags = "用户信息模块")
@RequestMapping("/api/ui/")
public class UserInfoController {

    @Resource
    private UserInfoService userInfoService;

    @ApiOperation(value = "根据用户编号查询用户详细信息")
    @GetMapping("/")
    public Message<UserInfo> getUserDetailInfoByUserId(String userId, HttpServletRequest req)
    {
        /*
        * 查询用户
        * 进行权限判断
        * token为空，报错
        * 如果是校友，只能查自己的信息
        * 如果是管理员，想查谁查谁
        * */
        TokenInfo info = JwtUtil.getTokenInfo(req.getHeader("token"));
        if (Integer.parseInt(info.getUserType()) >1 && StrUtil.isNotEmpty(userId)) {
            return userInfoService.getUserInfoByUserId(userId);
        } else {
            return userInfoService.getUserInfoByUserId(info.getUserId());
        }
    }

    @ApiOperation(value = "[需要token]根据用户班级名称查询用户信息")
    @GetMapping("/class")
    public Message<List<UserInfo>> getUserDetailInfoByUserClass(String userClassName){
        return userInfoService.getUserInfoByClassName(userClassName);
    }


    @ApiOperation(value = "[需要token]根据用户学部名称查询用户信息")
    @GetMapping("/part")
    public Message<List<UserInfo>> getUserDetailInfoByUserPart(String userPartName){
        return userInfoService.getUserInfoByPartName(userPartName);
    }

    @ApiOperation(value = "[需要token]小程序端更新用户通信信息")
    @PutMapping("/alumnus")
    public Message<?> updateUserInfoByUserId(String userId,String userPhone,String userEmail){
        return userInfoService.updateUserInfoByUserIdForAlumnus(userId, userPhone, userEmail);
    }

    @ApiOperation(value = "[需要token]管理端端更新用户全部详细信息")
    @PutMapping("/")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    public Message<?> updateUserInfoByUserId(UserInfo userInfo){
        //userInfo.setUserId(userId);
        return userInfoService.updateUserInfoByUserIdForAdmin(userInfo);
    }

    @ApiOperation(value = "根据用户token获取其所在学部的学生信息")
    @GetMapping("/partId")
    public Message<List<UserInfo>> getUserInfoByUserPartIdToken(HttpServletRequest req){
        TokenInfo info = JwtUtil.getTokenInfo(req.getHeader("token"));
        return userInfoService.getUserInfoByUserPartId(info.getUserPartId());
    }

    @ApiOperation(value = "条件查询用户信息")
    @GetMapping("/condition")
    public Message<List<UserInfo>> getUserInfoCondition(String userId,String userName,String className,String partName,String level,String phone){
        return userInfoService.getUserInfoConditionSearch(userId, userName, className, partName, level, phone);
    }




}
