package com.whw.alumnus.controller;

import com.whw.alumnus.entity.Message;
import com.whw.alumnus.entity.Moment;
import com.whw.alumnus.service.MomentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@Api(tags = "精彩瞬间模块")
@RequestMapping("api/moment")
public class MomentController {


    @Resource
    private MomentService momentService;



    @PostMapping("/addMoment")
    @ApiOperation(value = "发布精彩瞬间")
    public Message<Moment> addMoment(Moment moment){
        return momentService.addMoment(moment);
    }

    @GetMapping("/getMomentByMomentId/{momentId}")
    @ApiOperation(value = "根据瞬间Id查找瞬间")
    public Message<Moment> getMomentByMomentId(@PathVariable("momentId") int momentId){
        return momentService.getMomentByMomentId(momentId);
    }

    @PostMapping("/UpDateMomentStatusByMomentId/{momentId}")
    @ApiOperation(value = "根据瞬间Id,修改瞬间状态")
    public Message<Moment> UpDateMomentStatusByMomentId(@PathVariable("momentId")int momentId){
        return momentService.UpDateMomentStatusByMomentId(momentId);
    }

    @PostMapping("/deleteMomentByMomentId/{momentId}")
    @ApiOperation(value = "根据瞬间Id删除瞬间")
    public Message deleteMomentByMomentId(@PathVariable("momentId")int momentId){
        return momentService.deleteMomentByMomentId(momentId);
    }

    @GetMapping("/findAll")
    @ApiOperation(value = "获取所有瞬间信息")
    public Message findAll(){
        return momentService.getAllMoment();
    }

}
