package com.whw.alumnus.controller;

import com.whw.alumnus.entity.Message;
import com.whw.alumnus.entity.Star;
import com.whw.alumnus.service.StarService;
import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@Api(tags = "校园明星模块")
@RequestMapping("/api/star")
public class StarController {

    @Resource
    private StarService starService;

    @PostMapping("/add")
    @ApiOperation(value = "新增校园明星")

    public Message<Star> insertStar(Star star) {
        return starService.insertStar(star);
    }

    @GetMapping("/pass/part/{partId}")
    @ApiOperation(value = "通过学部编号查找状态为可见的校园明星")
    @JsonView(Star.StarSimpleView.class)
    public Message<List<Star>> getStarForAlumnus(@PathVariable("partId")String partId) {
        return starService.getStarByStarPartIdAndAndStarStatus(partId);
    }

    @PutMapping("/verify/{starId}")
    @ApiOperation(value = "发布明星根据编号(审核)")

    public Message<?> publishStarByStarId(@PathVariable("starId")int starId) {
        return starService.publishStarByStarId(starId);
    }

    @DeleteMapping("/{starId}")
    @ApiOperation(value = "根据编号删除校园明星")
    public Message<?> deleteStarByStarId(@PathVariable("starId")int starId) {
        return starService.deleteStarByStarId(starId);
    }

    @GetMapping("/pass/all")
    @ApiOperation(value = "查找所有状态为可见的校园明星")
    public Message<List<Star>> getAllStar() {
        return starService.getAllStar("1");
    }

    @GetMapping("/id/{starId}")
    @ApiOperation(value = "根据明星编号获取校园明星")
    public Message<Star> getStarByStarId(@PathVariable("starId")int starId) {
        return starService.getStarByStarId(starId);
    }


    @GetMapping("/part/{starPartId}")
    @ApiOperation(value = "根据学部编号获取校园明星")

    public Message<List<Star>> getStarByStarPartId(@PathVariable("starPartId") String starPartId) {
        return starService.getStarByStarPartId(starPartId);
    }

    @GetMapping("/status/{starStatus}")
    @ApiOperation(value = "根据可见状态获取校园明星")
    public Message<List<Star>> getStarByStarStatus(@PathVariable("starStatus")String starStatus) {
        return starService.getStarByStarStatus(starStatus);
    }

    @PutMapping("/like/{starId}")
    @ApiOperation(value = "根据校园明星编号更新点赞数量")

    public Message<?> updateStarLikeCountByStarId(@PathVariable("starId")int starId) {
        return starService.updateStarLikeCountByStarId(starId);
    }

    @GetMapping("/user/{starUserId}")
    @ApiOperation(value = "根据主人公编号获取校园明星")
    public Message<List<Star>> getStarByStarUserId(@PathVariable("starUserId") String starUserId) {
        return starService.getStarByStarUserId(starUserId);
    }

    @GetMapping("/userName/{starUserName}")
    @ApiOperation(value = "根据主人公姓名查找校园明星")
    public Message<List<Star>> getStartByStarUserName(@PathVariable("starUserName") String starUserName){
        return starService.getStarByStarUserName(starUserName);
    }

    @GetMapping("/condition")
    @ApiOperation(value = "条件查询明星")
    public Message<List<Star>> getStarConditionSearch(String starTitle, String starUserName, String starHost, String starUserPartName, String starUserClassName) {
        return starService.getStarConditionSearch(starTitle,starUserName,starHost,starUserPartName,starUserClassName);
    }

}
