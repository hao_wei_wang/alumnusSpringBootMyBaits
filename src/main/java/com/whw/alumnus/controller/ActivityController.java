package com.whw.alumnus.controller;

import cn.hutool.http.HttpRequest;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.whw.alumnus.entity.Activity;
import com.whw.alumnus.entity.Message;
import com.whw.alumnus.entity.TokenInfo;
import com.whw.alumnus.service.ActivityService;
import com.whw.alumnus.utils.JwtUtil;
import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@Api(tags = "校园活动模块")
@RequestMapping("api/activity")
public class ActivityController {

    @Resource
    private ActivityService activityService;

    @PostMapping("/add")
    @ApiOperation(value = "添加校园活动，初始状态为0，待审核,活动创建时间为当前时间")
    public Message addActivity(Activity activity, HttpServletRequest req){
        TokenInfo info = JwtUtil.getTokenInfo(req.getHeader("token"));
        activity.setActUserId(info.getUserId());
        activity.setActPartId(info.getUserPartId());
        activity.setActCreateTime(new Date());
        activity.setActStatus("0");
        return activityService.insertSelective(activity);
    }

    @GetMapping("/pass/part/{actPartId}")
    @ApiOperation(value = "通过学部编号查找已审核的校园活动")
    public Message selectByActPartIdAndActStatusNot(@PathVariable("actPartId")String actPartId){
        return activityService.selectByActPartIdAndActStatusNot(actPartId,"0");
    }


    @GetMapping("/user/{actUserId}")
    @ApiOperation(value = "通过创建人编号查找校园活动")
    public Message<List<Activity>> selectByActUserId(@PathVariable("actUserId")String actUserId){
        return activityService.selectByActUserId(actUserId);
    }


    @GetMapping("/pass/list/{actPartId}")
    @ApiOperation(value = "【小程序活动列表】通过学部编号查找已审核的校园活动 以及 活动类型为校级的已审核校园活动")
    public Message<List<Activity>> getActivitybyActType(@PathVariable("actPartId")String actPartId){
        return activityService.getActivityByActPartIdAndActType(actPartId);
    }

    @GetMapping("/user/status/{actUserId}/{actStatus}")
    @ApiOperation(value = "通过创建人和活动状态查找活动")
    public Message<List<Activity>> getActivitybyActUserIdAndActStatus(@PathVariable("actUserId")String actUserId,@PathVariable("actStatus")String actStuts){
        return activityService.getActivityByActUserIdAndActStatus(actUserId,actStuts);
    }

    @GetMapping("/part/{actPartId}")
    @ApiOperation(value = "通过学部编号查找所有校园活动")
    @JsonView(Activity.ActivitySimpleView.class)
    public Message<List<Activity>> getAllActivitybyActPartId(@PathVariable("actPartId")String actPartId){
        return activityService.getAllActivityByActPartId(actPartId);
    }

    @GetMapping("/all")
    @ApiOperation(value = "查找所有活动")
    @JsonView(Activity.ActivitySimpleView.class)
    public Message<List<Activity>> getAllActivity(){
        return activityService.getAllActivity();
    }

    @GetMapping("/status/{actStatus}")
    @ApiOperation(value = "通过活动状态查找活动")
    public Message<List<Activity>> getActivitybyActStatus(@PathVariable("actStatus")String actStatus){
        return activityService.getActivityByActStatus(actStatus);
    }

    @GetMapping("/type/{actType}")
    @ApiOperation(value = "通过活动类型查找活动")
    public Message<List<Activity>> getActivityByActType(@PathVariable("actType")String actType){
        return activityService.getActivityByActType(actType);
    }

    @GetMapping("/typeAndPartId")
    @ApiOperation(value = "根据学部编号以及活动类型查询活动")
    public Message<List<Activity>> getActivityByActTypeAndActPartId(String actStatus,HttpServletRequest req){
        TokenInfo info = JwtUtil.getTokenInfo(req.getHeader("token"));
        return activityService.getActivityByActStatusAndActPartId(actStatus,info.getUserPartId());
    }

    @GetMapping("/id/{actId}")
    @ApiOperation(value = "通过活动Id查找活动")
    public Message<Activity> getActivityByActId(@PathVariable("actId")int actId){
        return activityService.getActivityByActId(actId);
    }

    @PutMapping("/verify/{actId}")
    @ApiOperation(value = "审核活动（根据活动编号将活动状态改为1）@UseToken(level=3)")
    public Message<?> auditActivityByActId(@PathVariable("actId")int actId){
        return activityService.updateActivityByActId("1",actId);
    }

    @PutMapping("/status/{actId}/{actStatus}")
    @ApiOperation(value = "修改活动状态")
    public Message<?> auditActivityByActId(@PathVariable("actStatus")String actStatus,@PathVariable("actId")int actId){
        return activityService.updateActivityByActId(actStatus,actId);
    }
    @GetMapping("/address")
    @ApiOperation(value="根据地址获得经纬度,返回 obj:{经度;纬度}")
    public Message<?> getCoordinateByAddress(String address){
        Map paramMap = new HashMap<>();
        paramMap.put("output","json");
        paramMap.put("ak","TFcnsebs1M7X73ON7ZXCfGvFxjq72l0A");
        paramMap.put("address",address);
        String rs= HttpRequest.get("http://api.map.baidu.com/geocoder/v2/")
                .form(paramMap)
                .execute().body();
        if (JSONUtil.isJson(rs)){
            JSONObject jsonObject = JSONUtil.parseObj(rs);
            System.out.println(jsonObject.toString());
            if (jsonObject.getStr("status").equals("0")) {
                return Message.success(null).add(jsonObject.getJSONObject("result").getJSONObject("location").getStr("lng")+";"+jsonObject.getJSONObject("result").getJSONObject("location").getStr("lat"));
            }else {
                return Message.fail("您的位置描述不清晰");
            }
        }
        return Message.fail(null);
    }

    @GetMapping("/titly/{actTitly}")
    @ApiOperation(value = "根据活动标题查找活动")
    public Message getActivityByAvtivityTitly(@PathVariable("actTitly") String ActivityTitly){
        return activityService.getActivityByActTitly(ActivityTitly);
    }

    @GetMapping("/condition")
    @ApiOperation(value = "条件查询活动")
    public Message<List<Activity>> getActivityConditionSearch(String actTitle,String actPrincipalName,String actAddress){
        return activityService.getActivityConditionSearch(actTitle,actPrincipalName,actAddress);
    }

    @ApiOperation(value = "根据jobId删除")
    @DeleteMapping("/")
    public Message deleteByActivityId(int ActivityId){
        return activityService.deleteByActivityId(ActivityId);
    }

}
