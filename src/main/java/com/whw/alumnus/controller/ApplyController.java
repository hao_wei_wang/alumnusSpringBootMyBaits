package com.whw.alumnus.controller;

import com.whw.alumnus.entity.Apply;
import com.whw.alumnus.entity.Message;
import com.whw.alumnus.service.ApplyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;

@RestController
@Api(tags = "活动报名模块")
@RequestMapping("/api/apply")
public class ApplyController {

    @Resource
    private ApplyService applyService;

    @PostMapping("/")
    @ApiOperation(value = "添加报名信息，并根据活动的录取形式，生成报名状态（若报名不须审核则直接录取状态）")
    public Message<Apply> insertApply(Apply apply){
        return applyService.insertApply(apply);
    }

    @GetMapping("/search/status/{ApplyActivityId}")
    @ApiOperation(value = "根据活动编号查询已录取的报名信息")
    public Message<List<Apply>> getApplyByApplyActivityIdAndApplyStatus(@PathVariable("ApplyActivityId")String ApplyActivityId){
        return applyService.getApplyByApplyActivityIdAndApplyStatus(ApplyActivityId);
    }

    @GetMapping("/search/activityId/{activityId}")
    @ApiOperation(value = "根据活动编号查询所有的报名信息")
    public Message<List<Apply>> getApplyByActivityId(@PathVariable("activityId")String activityId){
        return applyService.getApplyByActivityId(activityId);
    }

    @GetMapping("/search/applyId/{applyId}")
    @ApiOperation(value = "根据报名编号查询所有的报名详细信息")
    public Message<Apply> getApplyByApplyId(@PathVariable("applyId")int applyId){
        return applyService.getApplyByApplyId(applyId);
    }

    @GetMapping("/search/ApplyUserId/{ApplyUserId}")
    @ApiOperation(value = "根据用户编号查询所有的报名信息")
    public Message<List<Apply>> getApplyByApplyUserId(@PathVariable("ApplyUserId")String ApplyUserId){
        return applyService.getApplyByApplyUserId(ApplyUserId);
    }

    @GetMapping("/search/userId/status/{ApplyUserId}/{ApplyStatus}")
    @ApiOperation(value = "通过用户编号查询和录取状态查询报名信息")
    public Message getApplyByApplyUserIdAndApplyStatus(@PathVariable("ApplyUserId")String ApplyUserId,@PathVariable("ApplyStatus")String ApplyStatus){
        return applyService.getApplyByApplyUserIdAndApplyStatus(ApplyUserId,ApplyStatus);
    }

    @GetMapping("/count/ApplyId/{ApplyActivityId}")
    @ApiOperation(value = "通过活动编号统计报名数量")
    public Message countApplyByApplyId(@PathVariable("ApplyActivityId")String ApplyActivityId){
        return applyService.countApplyByApplyActivityId(ApplyActivityId);
    }

    @GetMapping("/count/ApplyId/Status/{ApplyActivityId}")
    @ApiOperation(value = "通过活动编号统计已录取的报名数量")
    public Message countApplyByApplyIdAndApplyStatus(@PathVariable("ApplyActivityId")String ApplyActivityId){
        return applyService.countApplyByApplyActivityIdAndApplyStatus(ApplyActivityId);
    }

    @GetMapping("/upData/{ApplyActivityId}/{ApplyUserId}")
    @ApiOperation(value = "通过活动编号、用户编号修改录取状态")
    public Message UpdateApplyStatusByApplyActivityIdAndApplyUserId(@PathVariable("ApplyActivityId")String ApplyActivityId,@PathVariable("ApplyUserId")String AppplyUserId){
        return applyService.UpdateApplyStatusByApplyActivityIdAndApplyUserId(ApplyActivityId,AppplyUserId);
    }

    @GetMapping("/outExcel/{ApplyActivityId}")
    @ApiOperation(value = "根据活动编号将已录取的报名记录导出为Excel")
    public Message OutPutExcel(@PathVariable("ApplyActivityId")String ActivityId) throws IOException {
        return applyService.OutExcel(ActivityId);
    }

}
