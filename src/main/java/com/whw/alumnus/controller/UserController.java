package com.whw.alumnus.controller;

import com.whw.alumnus.entity.Message;
import com.whw.alumnus.entity.TokenInfo;
import com.whw.alumnus.entity.User;
import com.whw.alumnus.service.UserService;
import com.whw.alumnus.utils.JwtUtil;
import io.jsonwebtoken.Claims;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@Api(tags = "用户模块")
@RequestMapping("/api/user")
public class UserController {

    @Resource
    private UserService userService;

    @PostMapping("/token")
    @ApiOperation(value = "使用用户名和密码获取用户token")
    public Message userLogin(String userId, String password){
        return userService.userLoginByUserIdAndPassword(userId, password);
    }

    @GetMapping("/")
    @ApiOperation(value = "获取当前token用户的信息")
    public Message<User> getUserByToken(String token)  {
        if (token != null) {
            try {
                Claims claims = JwtUtil.resolveToken(token);
                String userId = (String)claims.get("userId");
                return userService.searchUserInfoByUserId(userId);
            }catch (Exception e){
                return Message.fail("登录已过期，请重新登录");
            }
        }
        return Message.fail("token不存在");
    }

    @GetMapping("/token")
    @ApiOperation(value = "使用小程序jsCode获取用户token")
    public Message userLogin(String jsCode){
        return userService.userLoginByJsCode(jsCode);
    }

    @PostMapping("/register")
    @ApiOperation(value = "新增管理端用户")
    public Message managerUserRegister(String userId,String userName,String password,String partId,String type){
        return userService.insertManageUser(userId, userName, password, partId, type);
    }

    @GetMapping("/class/{classId}")
    @ApiOperation(value = "根据班级编号获取用户")
    public Message<List<User>> getUserByClassId(@PathVariable("classId") String classId){
        return userService.getUserByClassId(classId);
    }

    @GetMapping("/part/{partId}")
    @ApiOperation(value = "根据学部编号获取用户")
    public Message<List<User>> getUserByPartId(@PathVariable("partId") String partId)
    {
        return userService.getUserByPartId(partId);
    }

    @PutMapping("bind")
    @ApiOperation(value = "根据token绑定OpenId")
    public Message<?> bindOpenIdByToken(String jsCode,String token){
        TokenInfo tokenInfo = JwtUtil.getTokenInfo(token);
        return userService.bindUserIdAndOpenId(tokenInfo.getUserId(), jsCode);
    }


    @GetMapping("/logout")
    @ApiOperation(value = "无意义注销接口")
    public Message logout()
    {
        return Message.success(null);
    }








}
