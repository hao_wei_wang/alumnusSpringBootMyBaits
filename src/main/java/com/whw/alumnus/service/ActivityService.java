package com.whw.alumnus.service;

import com.whw.alumnus.entity.Activity;
import com.whw.alumnus.entity.Message;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ActivityService{


    Message deleteByPrimaryKey(Integer actId);

    Message insertSelective(Activity record);

    Message selectByPrimaryKey(Integer actId);

    Message updateByPrimaryKeySelective(Activity record);

    Message selectByActPartIdAndActStatusNot(@Param("actPartId")String actPartId, @Param("actStatus")String actStatus);

    Message selectByActUserId(@Param("actUserId")String actUserId);
}
