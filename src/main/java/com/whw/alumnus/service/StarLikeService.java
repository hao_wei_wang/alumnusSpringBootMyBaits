package com.whw.alumnus.service;

import com.whw.alumnus.entity.Message;
import com.whw.alumnus.entity.StarLike;
public interface StarLikeService{


    Message deleteByPrimaryKey(Integer id);

    Message insertSelective(StarLike record);

    Message selectByPrimaryKey(Integer id);

    Message updateByPrimaryKeySelective(StarLike record);

}
