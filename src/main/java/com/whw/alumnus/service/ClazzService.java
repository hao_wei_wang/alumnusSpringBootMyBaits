package com.whw.alumnus.service;

import com.whw.alumnus.entity.Clazz;
import com.whw.alumnus.entity.Message;

public interface ClazzService{


    Message deleteByPrimaryKey(Integer clazzId);

    Message insertSelective(Clazz record);

    Message selectByPrimaryKey(Integer clazzId);

    Message updateByPrimaryKeySelective(Clazz record);

}
