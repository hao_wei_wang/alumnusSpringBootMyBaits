package com.whw.alumnus.service;

import com.whw.alumnus.entity.Apply;
import com.whw.alumnus.entity.Message;

public interface ApplyService{


    Message deleteByPrimaryKey(Integer applyId);

    Message insertSelective(Apply record);

    Message selectByPrimaryKey(Integer applyId);

    Message updateByPrimaryKeySelective(Apply record);

}
