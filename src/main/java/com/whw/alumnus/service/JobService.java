package com.whw.alumnus.service;

import com.whw.alumnus.entity.Job;
import com.whw.alumnus.entity.Message;

public interface JobService{


    Message deleteByPrimaryKey(Integer jobId);

    Message insertSelective(Job record);

    Message selectByPrimaryKey(Integer jobId);

    Message updateByPrimaryKeySelective(Job record);

}
