package com.whw.alumnus.service;

import com.whw.alumnus.entity.Message;
import com.whw.alumnus.entity.News;
public interface NewsService{


    Message deleteByPrimaryKey(Integer newsId);

    Message insertSelective(News record);

    Message selectByPrimaryKey(Integer newsId);

    Message updateByPrimaryKeySelective(News record);

}
