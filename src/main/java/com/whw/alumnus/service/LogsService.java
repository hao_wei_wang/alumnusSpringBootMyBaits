package com.whw.alumnus.service;

import com.whw.alumnus.entity.Logs;
import com.whw.alumnus.entity.Message;

public interface LogsService{


    Message deleteByPrimaryKey(Integer logsId);

    Message insertSelective(Logs record);

    Message selectByPrimaryKey(Integer logsId);

    Message updateByPrimaryKeySelective(Logs record);

}
