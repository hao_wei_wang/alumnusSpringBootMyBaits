package com.whw.alumnus.service;

import com.whw.alumnus.entity.Message;
import com.whw.alumnus.entity.Moment;
public interface MomentService{


    Message deleteByPrimaryKey(Integer momentId);

    Message insertSelective(Moment record);

    Message selectByPrimaryKey(Integer momentId);

    Message updateByPrimaryKeySelective(Moment record);

}
