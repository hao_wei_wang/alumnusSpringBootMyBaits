package com.whw.alumnus.service.impl;

import com.whw.alumnus.entity.Message;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.whw.alumnus.mapper.KnowledgeMapper;
import com.whw.alumnus.entity.Knowledge;
import com.whw.alumnus.service.KnowledgeService;
@Service
public class KnowledgeServiceImpl implements KnowledgeService{

    @Resource
    private KnowledgeMapper knowledgeMapper;

    @Override
    public Message deleteByPrimaryKey(Integer knowledgeId) {
        return knowledgeMapper.deleteByPrimaryKey(knowledgeId)>0?Message.success(null):Message.fail(null);
    }

    @Override
    public Message insertSelective(Knowledge record) {
        return knowledgeMapper.insertSelective(record)>0?Message.success(null):Message.fail(null);
    }

    @Override
    public Message selectByPrimaryKey(Integer knowledgeId) {
        return Message.success(null).add(knowledgeMapper.selectByPrimaryKey(knowledgeId));
    }

    @Override
    public Message updateByPrimaryKeySelective(Knowledge record) {
        return knowledgeMapper.updateByPrimaryKeySelective(record)>0?Message.success(null):Message.fail(null);
    }

}
