package com.whw.alumnus.service.impl;

import com.whw.alumnus.entity.Message;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.whw.alumnus.mapper.UserMapper;
import com.whw.alumnus.entity.User;
import com.whw.alumnus.service.UserService;
@Service
public class UserServiceImpl implements UserService{

    @Resource
    private UserMapper userMapper;

    @Override
    public Message deleteByPrimaryKey(String userId) {
        return userMapper.deleteByPrimaryKey(userId)>0?Message.success(null):Message.fail(null);
    }

    @Override
    public Message insertSelective(User record) {
        return userMapper.insertSelective(record)>0?Message.success(null):Message.fail(null);
    }

    @Override
    public Message selectByPrimaryKey(String userId) {
        return Message.success(null).add(userMapper.selectByPrimaryKey(userId));
    }

    @Override
    public Message updateByPrimaryKeySelective(User record) {
        return userMapper.updateByPrimaryKeySelective(record)>0?Message.success(null):Message.fail(null);
    }

}
