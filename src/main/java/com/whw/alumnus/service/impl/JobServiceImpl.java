package com.whw.alumnus.service.impl;

import com.whw.alumnus.entity.Message;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.whw.alumnus.mapper.JobMapper;
import com.whw.alumnus.entity.Job;
import com.whw.alumnus.service.JobService;
@Service
public class JobServiceImpl implements JobService{

    @Resource
    private JobMapper jobMapper;

    @Override
    public Message deleteByPrimaryKey(Integer jobId) {
        return jobMapper.deleteByPrimaryKey(jobId)>0? Message.success(null):Message.fail(null);
    }

    @Override
    public Message insertSelective(Job record) {
        return jobMapper.insertSelective(record)>0?Message.success(null):Message.fail(null);
    }

    @Override
    public Message selectByPrimaryKey(Integer jobId) {
        return Message.success(null).add(jobMapper.selectByPrimaryKey(jobId));
    }

    @Override
    public Message updateByPrimaryKeySelective(Job record) {
        return jobMapper.updateByPrimaryKeySelective(record)>0?Message.success(null):Message.fail(null);
    }

}
