package com.whw.alumnus.service.impl;

import com.whw.alumnus.entity.Message;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.whw.alumnus.mapper.MomentMapper;
import com.whw.alumnus.entity.Moment;
import com.whw.alumnus.service.MomentService;
@Service
public class MomentServiceImpl implements MomentService{

    @Resource
    private MomentMapper momentMapper;

    @Override
    public Message deleteByPrimaryKey(Integer momentId) {
        return momentMapper.deleteByPrimaryKey(momentId)>0?Message.success(null):Message.fail(null);
    }

    @Override
    public Message insertSelective(Moment record) {
        return momentMapper.insertSelective(record)>0?Message.success(null):Message.fail(null);
    }

    @Override
    public Message selectByPrimaryKey(Integer momentId) {
        return Message.success(null).add(momentMapper.selectByPrimaryKey(momentId));
    }

    @Override
    public Message updateByPrimaryKeySelective(Moment record) {
        return momentMapper.updateByPrimaryKeySelective(record)>0?Message.success(null):Message.fail(null);
    }

}
