package com.whw.alumnus.service.impl;

import com.whw.alumnus.entity.Message;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.whw.alumnus.mapper.NewsMapper;
import com.whw.alumnus.entity.News;
import com.whw.alumnus.service.NewsService;
@Service
public class NewsServiceImpl implements NewsService{

    @Resource
    private NewsMapper newsMapper;

    @Override
    public Message deleteByPrimaryKey(Integer newsId) {
        return newsMapper.deleteByPrimaryKey(newsId)>0?Message.success(null):Message.fail(null);
    }

    @Override
    public Message insertSelective(News record) {
        return newsMapper.insertSelective(record)>0?Message.success(null):Message.fail(null);
    }

    @Override
    public Message selectByPrimaryKey(Integer newsId) {
        return Message.success(null).add(newsMapper.selectByPrimaryKey(newsId));
    }

    @Override
    public Message updateByPrimaryKeySelective(News record) {
        return newsMapper.updateByPrimaryKeySelective(record)>0?Message.success(null):Message.fail(null);
    }

}
