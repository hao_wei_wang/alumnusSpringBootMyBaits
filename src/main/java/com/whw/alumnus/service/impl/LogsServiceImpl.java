package com.whw.alumnus.service.impl;

import com.whw.alumnus.entity.Message;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.whw.alumnus.entity.Logs;
import com.whw.alumnus.mapper.LogsMapper;
import com.whw.alumnus.service.LogsService;
@Service
public class LogsServiceImpl implements LogsService{

    @Resource
    private LogsMapper logsMapper;

    @Override
    public Message deleteByPrimaryKey(Integer logsId) {
        return logsMapper.deleteByPrimaryKey(logsId)>0?Message.success(null):Message.fail(null);
    }

    @Override
    public Message insertSelective(Logs record) {
        return logsMapper.insertSelective(record)>0?Message.success(null):Message.fail(null);
    }

    @Override
    public Message selectByPrimaryKey(Integer logsId) {
        return Message.success(null).add(logsMapper.selectByPrimaryKey(logsId));
    }

    @Override
    public Message updateByPrimaryKeySelective(Logs record) {
        return logsMapper.updateByPrimaryKeySelective(record)>0?Message.success(null):Message.fail(null);
    }

}
