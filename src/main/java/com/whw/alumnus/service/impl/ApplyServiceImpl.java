package com.whw.alumnus.service.impl;

import com.whw.alumnus.entity.Message;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.whw.alumnus.entity.Apply;
import com.whw.alumnus.mapper.ApplyMapper;
import com.whw.alumnus.service.ApplyService;
@Service
public class ApplyServiceImpl implements ApplyService{

    @Resource
    private ApplyMapper applyMapper;

    @Override
    public Message deleteByPrimaryKey(Integer applyId) {
        return applyMapper.deleteByPrimaryKey(applyId)>0?Message.success(null):Message.fail(null);
    }

    @Override
    public Message insertSelective(Apply record) {
        return applyMapper.insertSelective(record)>0?Message.success(null):Message.fail(null);
    }

    @Override
    public Message selectByPrimaryKey(Integer applyId) {
        return Message.success(null).add(applyMapper.selectByPrimaryKey(applyId));
    }

    @Override
    public Message updateByPrimaryKeySelective(Apply record) {
        return applyMapper.updateByPrimaryKeySelective(record)>0?Message.success(null):Message.fail(null);
    }

}
