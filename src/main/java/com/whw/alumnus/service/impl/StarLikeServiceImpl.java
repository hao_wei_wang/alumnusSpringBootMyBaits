package com.whw.alumnus.service.impl;

import com.whw.alumnus.entity.Message;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.whw.alumnus.mapper.StarLikeMapper;
import com.whw.alumnus.entity.StarLike;
import com.whw.alumnus.service.StarLikeService;
@Service
public class StarLikeServiceImpl implements StarLikeService{

    @Resource
    private StarLikeMapper starLikeMapper;

    @Override
    public Message deleteByPrimaryKey(Integer id) {
        return starLikeMapper.deleteByPrimaryKey(id)>0?Message.success(null):Message.fail(null);
    }

    @Override
    public Message insertSelective(StarLike record) {
        return starLikeMapper.insertSelective(record)>0?Message.success(null):Message.fail(null);
    }

    @Override
    public Message selectByPrimaryKey(Integer id) {
        return Message.success(null).add(starLikeMapper.selectByPrimaryKey(id));
    }

    @Override
    public Message updateByPrimaryKeySelective(StarLike record) {
        return starLikeMapper.updateByPrimaryKeySelective(record)>0?Message.success(null):Message.fail(null);
    }

}
