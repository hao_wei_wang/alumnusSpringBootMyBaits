package com.whw.alumnus.service.impl;

import com.whw.alumnus.entity.Message;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.whw.alumnus.mapper.PartMapper;
import com.whw.alumnus.entity.Part;
import com.whw.alumnus.service.PartService;
@Service
public class PartServiceImpl implements PartService{

    @Resource
    private PartMapper partMapper;

    @Override
    public Message deleteByPrimaryKey(Integer partId) {
        return partMapper.deleteByPrimaryKey(partId)>0?Message.success(null):Message.fail(null);
    }

    @Override
    public Message insertSelective(Part record) {
        return partMapper.insertSelective(record)>0?Message.success(null):Message.fail(null);
    }

    @Override
    public Message selectByPrimaryKey(Integer partId) {
        return Message.success(null).add(partMapper.selectByPrimaryKey(partId));
    }

    @Override
    public Message updateByPrimaryKeySelective(Part record) {
        return partMapper.updateByPrimaryKeySelective(record)>0?Message.success(null):Message.fail(null);
    }

}
