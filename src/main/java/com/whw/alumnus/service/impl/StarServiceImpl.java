package com.whw.alumnus.service.impl;

import com.whw.alumnus.entity.Message;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.whw.alumnus.mapper.StarMapper;
import com.whw.alumnus.entity.Star;
import com.whw.alumnus.service.StarService;
@Service
public class StarServiceImpl implements StarService{

    @Resource
    private StarMapper starMapper;

    @Override
    public Message deleteByPrimaryKey(Integer starId) {
        return starMapper.deleteByPrimaryKey(starId)>0?Message.success(null):Message.fail(null);
    }

    @Override
    public Message insertSelective(Star record) {
        return starMapper.insertSelective(record)>0?Message.success(null):Message.fail(null);
    }

    @Override
    public Message selectByPrimaryKey(Integer starId) {
        return Message.success(null).add(starMapper.selectByPrimaryKey(starId));
    }

    @Override
    public Message updateByPrimaryKeySelective(Star record) {
        return starMapper.updateByPrimaryKeySelective(record)>0?Message.success(null):Message.fail(null);
    }

}
