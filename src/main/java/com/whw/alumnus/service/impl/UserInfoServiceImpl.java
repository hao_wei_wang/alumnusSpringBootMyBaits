package com.whw.alumnus.service.impl;

import com.whw.alumnus.entity.Message;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.whw.alumnus.entity.UserInfo;
import com.whw.alumnus.mapper.UserInfoMapper;
import com.whw.alumnus.service.UserInfoService;
@Service
public class UserInfoServiceImpl implements UserInfoService{

    @Resource
    private UserInfoMapper userInfoMapper;

    @Override
    public Message deleteByPrimaryKey(String userId) {
        return userInfoMapper.deleteByPrimaryKey(userId)>0?Message.success(null):Message.fail(null);
    }

    @Override
    public Message insertSelective(UserInfo record) {
        return userInfoMapper.insertSelective(record)>0?Message.success(null):Message.fail(null);
    }

    @Override
    public Message selectByPrimaryKey(String userId) {
        return Message.success(null).add(userInfoMapper.selectByPrimaryKey(userId));
    }

    @Override
    public Message updateByPrimaryKeySelective(UserInfo record) {
        return userInfoMapper.updateByPrimaryKeySelective(record)>0?Message.success(null):Message.fail(null);
    }

}
