package com.whw.alumnus.service.impl;

import com.whw.alumnus.entity.Message;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.whw.alumnus.mapper.ClazzMapper;
import com.whw.alumnus.entity.Clazz;
import com.whw.alumnus.service.ClazzService;
@Service
public class ClazzServiceImpl implements ClazzService{

    @Resource
    private ClazzMapper clazzMapper;

    @Override
    public Message deleteByPrimaryKey(Integer clazzId) {
        return clazzMapper.deleteByPrimaryKey(clazzId)>0?Message.success(null):Message.fail(null);
    }

    @Override
    public Message insertSelective(Clazz record) {
        return clazzMapper.insertSelective(record)>0?Message.success(null):Message.fail(null);
    }

    @Override
    public Message selectByPrimaryKey(Integer clazzId) {
        return Message.success(null).add(clazzMapper.selectByPrimaryKey(clazzId));
    }

    @Override
    public Message updateByPrimaryKeySelective(Clazz record) {
        return clazzMapper.updateByPrimaryKeySelective(record)>0?Message.success(null):Message.fail(null);
    }

}
