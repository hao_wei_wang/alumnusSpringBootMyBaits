package com.whw.alumnus.service.impl;

import com.whw.alumnus.entity.Message;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.whw.alumnus.mapper.ActivityMapper;
import com.whw.alumnus.entity.Activity;
import com.whw.alumnus.service.ActivityService;
@Service
public class ActivityServiceImpl implements ActivityService{

    @Resource
    private ActivityMapper activityMapper;

    @Override
    public Message deleteByPrimaryKey(Integer actId) {
        return activityMapper.deleteByPrimaryKey(actId)>0?Message.success(null):Message.fail(null);
    }

    @Override
    public Message insertSelective(Activity record) {
        return activityMapper.insertSelective(record)>0?Message.success(null):Message.fail(null);
    }

    @Override
    public Message selectByPrimaryKey(Integer actId) {
        return Message.success(null).add(activityMapper.selectByPrimaryKey(actId));
    }

    @Override
    public Message updateByPrimaryKeySelective(Activity record) {
        return activityMapper.updateByPrimaryKeySelective(record)>0?Message.success(null):Message.fail(null);
    }

    @Override
    public Message selectByActPartIdAndActStatusNot(String actPartId, String actStatus) {
        return  Message.success(null).add(activityMapper.selectByActPartIdAndActStatusNot(actPartId, actStatus));
    }

    @Override
    public Message selectByActUserId(String actUserId) {
        return  Message.success(null).add(activityMapper.selectByActUserId(actUserId));
    }

}
