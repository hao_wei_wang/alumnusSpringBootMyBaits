package com.whw.alumnus.service;

import com.whw.alumnus.entity.Message;
import com.whw.alumnus.entity.User;
public interface UserService{


    Message deleteByPrimaryKey(String userId);

    Message insertSelective(User record);

    Message selectByPrimaryKey(String userId);

    Message updateByPrimaryKeySelective(User record);

}
