package com.whw.alumnus.service;

import com.whw.alumnus.entity.Message;
import com.whw.alumnus.entity.UserInfo;
public interface UserInfoService{


    Message deleteByPrimaryKey(String userId);

    Message insertSelective(UserInfo record);

    Message selectByPrimaryKey(String userId);

    Message updateByPrimaryKeySelective(UserInfo record);

}
