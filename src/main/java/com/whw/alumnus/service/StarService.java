package com.whw.alumnus.service;

import com.whw.alumnus.entity.Message;
import com.whw.alumnus.entity.Star;
public interface StarService{


    Message deleteByPrimaryKey(Integer starId);

    Message insertSelective(Star record);

    Message selectByPrimaryKey(Integer starId);

    Message updateByPrimaryKeySelective(Star record);

}
