package com.whw.alumnus.service;

import com.whw.alumnus.entity.Message;
import com.whw.alumnus.entity.Part;
public interface PartService{


    Message deleteByPrimaryKey(Integer partId);

    Message insertSelective(Part record);

    Message selectByPrimaryKey(Integer partId);

    Message updateByPrimaryKeySelective(Part record);

}
