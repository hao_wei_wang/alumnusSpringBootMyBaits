package com.whw.alumnus.service;

import com.whw.alumnus.entity.Knowledge;
import com.whw.alumnus.entity.Message;

public interface KnowledgeService{


    Message deleteByPrimaryKey(Integer knowledgeId);

    Message insertSelective(Knowledge record);

    Message selectByPrimaryKey(Integer knowledgeId);

    Message updateByPrimaryKeySelective(Knowledge record);

}
