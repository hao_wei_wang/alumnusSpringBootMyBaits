package com.whw.alumnus.utils;

import cn.hutool.http.HttpRequest;

import java.util.HashMap;
import java.util.Map;

public class WeChatUtils {
    /**
     * @Author: Wls
     * @Date: 1:13 2020/2/12
     * @Description: 微信登录
     */
    public static String WechatLoginUtil(String js_code,String appId,String secret)
    {
        Map data=new HashMap<>();
        data.put("appid",appId);
        data.put("secret",secret);
        data.put("js_code",js_code);
        data.put("grant_type","authorization_code");

        String result= HttpRequest.get("https://api.weixin.qq.com/sns/jscode2session")
                .form(data)
                .execute()
                .body();
        return result;
    }
}
